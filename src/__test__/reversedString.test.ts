import { reversedString } from '@/battles/reversedString'

describe("Passing a string a return it reversed", () => {
	test('string to reversed string', () => {
		expect(reversedString('world')).toBe('dlrow')
		expect(reversedString('hello')).toBe('olleh')
		expect(reversedString('')).toBe('')
		expect(reversedString('h')).toBe('h')
		expect(reversedString('potato')).not.toBe('potato')
	})
})