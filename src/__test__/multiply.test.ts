import * as x from '@/battles/multiply'

describe("Multiplying in different ways", () => {
	test("with a for loop", () => {
		expect(x.forMultiply(50, -50)).toBe(-2500)
	})

	test("with a while loo", () => {
		expect(x.whileMultiply(50, 50)).toBe(2500)
	})

	test("with a string", () => {
		expect(x.stringMultiply(2, 4)).toBe(8)
	})

	test("with a recursive function", () => {
		expect(x.recursiveMultiply(2, 100)).toBe(200)
	})

	test("with a division", () => {
		expect(x.divideMultiply(5, -5)).toBe(-25)
	})
})
