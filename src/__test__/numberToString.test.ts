import { numberToString } from '@/battles/numberToString'

describe("Passing a number and returning a string", () => {
  test('number to string', () => {
    expect(numberToString(67)).toBe('67')
    expect(numberToString(100.59)).toBe('100.59')
  }) 
})
