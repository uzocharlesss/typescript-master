export function reversedString(str: string): string {
	let chain: string = ""
	for (let i = str.length  - 1; i >= 0; i--) chain += str[i]
	return chain
}
