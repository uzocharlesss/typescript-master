/**
 * Function that multiply both variables without using the multiply operator
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */

// First method: iterating with for loop
export const forMultiply = (a: number, b: number): number => {
	let result = 0
	const positive = Math.abs(b) == b
	for (let i = 0; i < Math.abs(b); i++) {
		result = positive ? result + a : result - a
	}
	return result
}

// Second method: iterating with while loop
export const whileMultiply = (a: number, b: number): number => {
	let result = 0
	while (b > 0) {
		result += a
		b--
	}
	return result
}

// Thrid method: hacking string
export const stringMultiply = (a: number, b: number): number => {
	return " ".repeat(a).repeat(b).length
}

// Fourth method: recursion
export const recursiveMultiply = (a: number, b: number): number => {
	if (b === 0) return 0
	if (b === 1) return a
	return a + recursiveMultiply(a, b - 1)
}

// Fifth method: dividing
export const divideMultiply = (a: number, b: number): number => {
	return a / (1 / b)
}
