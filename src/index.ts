#!/usr/bin/ts-node

import { numberToString } from './battles/numberToString'
import { reversedString } from './battles/reversedString'
import { makeA } from './helpers/makea'

console.log([numberToString(20), reversedString("hello"), makeA('haRDjoke')])
