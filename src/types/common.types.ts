import { 
	ComplicatedJoke as HardJoke, 
	Joke, 
	Quote, 
	Riddle 
} from '@models';

/** Fact types per kind of content */
export type Fact = Quote | Quote[] | Joke | HardJoke | Riddle | string
