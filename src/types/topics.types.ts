import { Fact } from "./common.types"

/** Recieve a string key value and returns a Fact (string | Quote |)*/
export type topicOption = {
	[key: string]: Fact
}
