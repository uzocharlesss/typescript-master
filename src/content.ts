import { 
  ComplicatedJoke as HardJoke, 
  Joke, 
  Quote, 
  Riddle 
} from '@models';

export const quote: Quote = {
  quote: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.',
  author: '@simmxns',
};

export const joke: Joke = {
  body: 'Ut enim ad minim',
  category: 'Simple joke',
};

export const hardJoke: HardJoke = {
  body: 'Ut enim ad minim veniam, quis nostrud exercitation.',
  category: 'Simple joke',
  level: 3,
};

export const riddle: Riddle = {
  riddle: 'Duis aute irure dolor in reprehenderit in voluptate velit esse.',
  answer: 'Search on the loremipsum generator',
};
