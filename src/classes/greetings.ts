import ps from "prompt-sync"
const prompt = ps()

class Greeter {
	private _name: string

	public requestName(): void {
		this._name = prompt("What's your name handsome?: ")
	}

	public greet(): string {
		return `Hello ${this._name}`
	}
	public formalGreet(): string {
		return `Greetings my lord ${this._name}`
	}
	public urbanGreet(): string {
		return `What's Popping ${this._name}`
	}
}

export default Greeter