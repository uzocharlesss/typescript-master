class User {
	private _name: string
	private _lastname: string
	private _age: number

	constructor(name: string, lastname: string, age: number) {
		this._name = name
		this._lastname = lastname
		this.age = age
	}

	get age(): number { return this._age }
	set age(age: number) {
		this._age = age
		if (age < 1) throw new Error("Invalid years old")
	}

	get fullName(): string { return `${this._name} ${this._lastname}` }
	set fullName(fullname: string) {
		let words = fullname.split(" ")
		this._name = words[0]
		this._lastname = words[1]
	}
}

export default User
