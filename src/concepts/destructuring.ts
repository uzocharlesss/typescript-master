interface IUser {
	firstname: string,
	age: number
}

interface INumber {
	"one": number,
	"two": number,
	"three": number,
}

interface IItem {
	id: number,
	title: string,
	qty: number
}

const user: IUser = {
	firstname: 'Simon',
	age: 19
}

const number: INumber = {
	"one": 1,
	"two": 2,
	"three": 3,
}

export const item: IItem = {
	id: 5,
	title: 'Lorem',
	qty: 200
}

export const itemDestructured = ({ id, title }: typeof item): void => console.log(id, title)
// function itemDestructured({ id, title } = item): void { console.log(id, title) }
// function itemDestructured({ id, title }: IItem): void { console.log(id, title) }
// function itemDestructured({ id, title }: any): void { console.log(id, title) }

// distructuringItem(item)
//console.log(user.age, user.firstname); 
const { firstname, age } = user 
const { one, two, three } = number
// console.log(age, firstname) //19, Simon
// console.log(one, two, three) // 1,2,3
