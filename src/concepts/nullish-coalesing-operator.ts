/* 
Logical operators
?? -> nullish coalescing operator
If the first value is "null or undefined" the variable will be the second statement
|| -> OR
If the first value is "false" the variable will be the second statement
&& -> AND
If the first value is "true" the variable will be the second statement
*/

/*
falsy -> false
In boolean context this values will be false
if (false)
if (null)
if (undefined)
if (0)
if (-0)
if (0n)
if (NaN)
if ("")

truthy -> true
In boolean context this values will be true
if (true)
if ({})
if ([])
if (42)
if ("0")
if ("false")
if (new Date())
if (-42)
if (12n)
if (3.14)
if (-3.14)
if (Infinity)
if (-Infinity)

nullish -> null or undefined
In boolean context it will always be false 
if (null)
if (undefined)
*/

type NullBoolean = boolean | number | null | undefined

const DEFAULT_COUNT = 10

export const nullCount: NullBoolean = null ?? DEFAULT_COUNT
//                  null or undefined ^
export const orCount: NullBoolean = true || DEFAULT_COUNT
//                            falsy ^
export const andCount: NullBoolean = false && DEFAULT_COUNT
//                            truthy ^

// console.table([{"??": nullCount, "||": orCount, "&&": andCount }])
