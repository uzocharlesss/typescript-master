// optional chaining ?
/*
Allow work with objects an arrays with many dimensions without worriding if some part
of this one are not declared
*/

export const obj = {
  prop1: 'value',
  prop2: {
    nested1: 10
  }
}

// optional calling ?

