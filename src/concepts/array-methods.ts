export const fruits: string[] = ["Banana", "Kiwi", "Apple", "Blueberry"]

// .map => iterate the array and create a new one from the passed statement
export const Xfruits = fruits.map(item => `${item}X`)

// .join() => joins all array elements into a string, its like toString() method just that this can specify a separator 
export let newfruits = fruits.join("/")
// console.log(newfruits) // "Banana/Kiwi/Apple/Blueberry" 

// pop => removes last element of an array
// pop() returns the removed value
fruits.pop() // "Blueberry"
// console.log(fruits) // ["Banana", "Kiwi", "Apple"]

// push => add new element at the end of an array
// push() returns the new length
fruits.push("Mango") // 5
// console.log(fruits) // ["Banana", "Kiwi", "Apple", "Blueberry", "Mango"]

// shift => removes the first element of an array
// shift() returns the removed value
fruits.shift() // "Banana"
// console.log(fruits) // ["Kiwi", "Apple", "Blueberry", "Mango"]

// unshift => add new element at the beginning of an array
// unshift() returns the new length
fruits.unshift("Lemon") // 5
// console.log(fruits) // ["Lemon", "Banana","Kiwi", "Apple", "Blueberry", "Mango"]

// changing elements
fruits[0] = "Pear"
// console.log(fruits) // ["Pear", "Kiwi", "Apple", "Blueberry"]
// delete => remove any element specify the index of its, but the removed is coverted to undefined
// delete returns a boolean, true if the specified element was deleted
delete fruits[2]
// console.log(fruits) // ["Banana", "Orange", , "Mango"] 

// concat => merge two or more arrays in one
const fruits2: Array<string> = new Array("Pomelo", "Coconut", "Durian");
export const newnewfruits = fruits.concat(fruits2)
// console.log(newnewfruits) // ["Banana", "Orange", "Apple", "Mango", "Pomelo", "Coconut", "Durian"]

// splice => add new items in specific pos in an array
// (2) first parameter define where the elements gonna be put it
// (0) second parameter defines how many elements will be removed
// (Tomato) last parameters define the elements to be added
fruits.splice(2, 0, "Tomato")
// console.log(fruits) // ["Banana", "Kiwi", "Tomato", "Apple", "Blueberry"]

// splice also can remove items
fruits.splice(0, 1)
// console.log(fruits) // ["Kiwi", "Apple", "Blueberry"]

// slice => slice a part of an array to a new array
export const slicedArray = fruits.slice(0, 2)
// console.log(slicedArray) // ["Banana", "Kiwi", "Apple" ]

// sort => sorts an array alphabetically
fruits.sort()
// console.log(fruits) // ["Apple", "Banana", "Blueberry", "Kiwi"]
// sorting an array with numbers
export const numbers: number[] = [2,84,21,52,1,16]
// ASC
// console.log(numbers.sort((a, b) => a - b)) // [1, 2, 16, 21, 52, 84]
// DESC
// console.log(numbers.sort((a, b) => b - a)) // [84, 52, 21, 16, 2, 1]

// reverse => reverse the elements in an array
// It can use it to sort an array in DESC order
fruits.sort();
fruits.reverse();
// console.log(fruits) // ["Kiwi", "Blueberry", "Banana", "Apple"] 

export {}